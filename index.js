import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import helmet from 'helmet';
import morgan from 'morgan';

import clientRoutes from './routes/client.js';
import managementRoutes from './routes/management.js';
import generalRoutes from './routes/general.js';
import salesRoutes from './routes/sales.js';

// CONFIGURE ENVIRONMENT VARS
dotenv.config();
const app = express();
app.use(express.json());
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));
app.use(morgan("common"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());


// ROUTES
app.use('/client', clientRoutes);
app.use('/general', generalRoutes);
app.use('/management', managementRoutes);
app.use('/sales', salesRoutes);


// MONGOOSE SETUP
const db_config = { useNewUrlParser: true, useUnifiedTopology: true };
const port = process.env.port || 9000;
mongoose.connect(process.env.MONGO_URL, db_config)
    .then(() => { app.listen(PORT, () => console.info(`Server Port ${PORT}`)) })
    .catch((err) => { console.error(`${err} did not connect succeffuly.`); });